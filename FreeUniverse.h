#ifndef FREEUNIVERSE_H
#define FREEUNIVERSE_H

#include <Ogre.h>
#include <OgreWindowEventUtilities.h>
#include <OISInputManager.h>
#include <OISKeyboard.h>
#include <OISMouse.h>
#include <CEGUI/CEGUI.h>
#include <CEGUI/RendererModules/Ogre/Renderer.h>
#include <map>
#include <set>

#include "ApplicationBase.h"
#include "universe/USimulator.h"

using std::map;
using std::set;
using std::string;

class FreeUniverse :
        public ApplicationBase,
        public Ogre::WindowEventListener,
        public OIS::KeyListener,
        public OIS::MouseListener
{
public:
    FreeUniverse();
    ~FreeUniverse();

    void setTime(const std::string& t);
    void setPlanets(set<string> planets);
    bool paused() const;

protected:
    // ApplicationBase
    virtual bool frameStarted(const Ogre::FrameEvent &event);
    virtual bool frameRenderingQueued(const Ogre::FrameEvent& event);
    virtual void setupListeners();
    virtual void createScene();

    Ogre::SceneNode* mCamCentre;

    // Ogre::WindowEventListener
    virtual void windowResized(Ogre::RenderWindow* window);
    virtual void windowClosed(Ogre::RenderWindow* window);
    // KeyListener interface
    virtual bool keyPressed(const OIS::KeyEvent &event);
    virtual bool keyReleased(const OIS::KeyEvent &event);
    // MouseListener interface
    virtual bool mouseMoved(const OIS::MouseEvent &event);
    virtual bool mousePressed(const OIS::MouseEvent &event, OIS::MouseButtonID id);
    virtual bool mouseReleased(const OIS::MouseEvent &event, OIS::MouseButtonID id);

    bool mbShutDown;
    bool mbPause;

    Ogre::Real mRotate;
    Ogre::Real mMove;
    Ogre::Vector3 mDirection;

protected:
    USimulator mUSimulator;

    // OIS
    bool setupOIS(Ogre::RenderWindow* window);
    bool unattachOIS();

    OIS::InputManager* mInputManager;
    OIS::Mouse* mMouse;
    OIS::Keyboard* mKeyboard;

    // CEGUI
    bool setupCEGUI();
    void setDetailVisible(bool setting);
    void fillDetail(const PlanetInfo &info);
    bool onPlanetSelected(const CEGUI::EventArgs& e);
    bool onAlterSpeed(const CEGUI::EventArgs& e);

    CEGUI::OgreRenderer* mRenderer;
    CEGUI::Window* mRootWin;
    CEGUI::Window* mTimeLabel;
    CEGUI::Window* mPlanetLabel;
    CEGUI::Window* mPlanetPanel;
    CEGUI::Spinner* mSpeedControl;
    CEGUI::Window* mDetailPanel;

    Ogre::SceneNode* createPlanet(const string& name);
    void updatePlanet(const string& planetName);
    void updateUniverse();
    enum QueryFlags {
        PLANET_MASK = 1 << 0,
        CURVE_MASK = 1 << 1,
    };
    map<string, bool> mUpdated;
    map<string, int> mMapping;
    Universe mUniverse;
    set<string> mPlanets;
    map<Ogre::SceneNode*, Ogre::SceneNode*> mPCurves;


    void prepareToChangeFollow(Ogre::SceneNode *target = NULL);
    void stepChangingFollow(float deltaTime = 0.005);
    bool mbFollow;
    Ogre::SceneNode *mFollowNode;

private:
    void createSphere(const string &strName, const PlanetInfo &info, const int nRings = 16, const int nSegments = 16);
};

#endif // FREEUNIVERSE_H
