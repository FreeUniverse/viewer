#include "ApplicationBase.h"

#include <OgreConfigFile.h>
#include <OgreRenderWindow.h>
#include <OgreSceneManager.h>
#include <OgreCamera.h>
#include <OgreViewport.h>

ApplicationBase::ApplicationBase()
    : mRoot(0),
      mResourcesCfg(Ogre::StringUtil::BLANK),
      mPluginsCfg(Ogre::StringUtil::BLANK),
      startTime("1950-1-1")
{
}

ApplicationBase::~ApplicationBase()
{
    delete mRoot;
}

bool ApplicationBase::frameRenderingQueued(const Ogre::FrameEvent& evt)
{
    if (mWindow->isClosed())
        return false;

    return true;
}

void ApplicationBase::createScene()
{
    mSceneMgr->setAmbientLight(Ogre::ColourValue(.15, .15, .15));
}

bool ApplicationBase::go()
{
#ifdef _DEBUG
    mResourcesCfg = "resources_d.cfg";
    mPluginsCfg = "plugins_d.cfg";
#else
    mResourcesCfg = "resources.cfg";
    mPluginsCfg = "plugins.cfg";
#endif

    if (!setup()) {
        return false;
    }

    createScene();

    mRoot->startRendering();

    return true;
}

bool ApplicationBase::setup()
{
    mRoot = new Ogre::Root(mPluginsCfg);

    loadResourceConfig();

    if (!setupConfig())
        return false;

    setupSceneManager();

    setupViewing();

    Ogre::TextureManager::getSingleton().setDefaultNumMipmaps(5);

    loadResource();

    setupListeners();

    return true;
}

void ApplicationBase::loadResourceConfig()
{
    Ogre::ConfigFile cf;
    cf.load(mResourcesCfg);

    Ogre::String secName, typeName, archName;
    Ogre::ConfigFile::SectionIterator secIt = cf.getSectionIterator();
    while (secIt.hasMoreElements()) {
        secName = secIt.peekNextKey();
        Ogre::ConfigFile::SettingsMultiMap* settings = secIt.getNext();
        Ogre::ConfigFile::SettingsMultiMap::iterator setIt;

        for (setIt = settings->begin(); setIt != settings->end(); ++setIt) {
            typeName = setIt->first;
            archName = setIt->second;
            Ogre::ResourceGroupManager::getSingleton().addResourceLocation(archName, typeName, secName);
        }
    }
}

void ApplicationBase::loadResource()
{
    // Initialising Resources
    Ogre::ResourceGroupManager::getSingleton().initialiseAllResourceGroups();
}

bool ApplicationBase::setupConfig()
{
    if (!(mRoot->restoreConfig() || mRoot->showConfigDialog()))
        return false;

    // Creating a RenderWindow
    mWindow = mRoot->initialise(true, "Free Universe");

    return true;
}

void ApplicationBase::setupSceneManager()
{
    // Creating a SceneManager
    mSceneMgr = mRoot->createSceneManager(Ogre::ST_GENERIC);
}

void ApplicationBase::setupViewing()
{
    // Creating the Camera
    mCamera = mSceneMgr->createCamera("MainCamera");

    mCamera->lookAt(0, 0, -300);

    mCamera->setNearClipDistance(5);

    bool infiniteClip =
      mRoot->getRenderSystem()->getCapabilities()->hasCapability(
        Ogre::RSC_INFINITE_FAR_PLANE);
    if (infiniteClip)
      mCamera->setFarClipDistance(0);
    else
      mCamera->setFarClipDistance(500000);

    Ogre::SceneNode* node = mSceneMgr->getRootSceneNode()->createChildSceneNode(
      "CameraNode", Ogre::Vector3(0, 0, 80000));
    mCamNode = node;
    node->attachObject(mCamera);

    Ogre::Viewport* vp = mWindow->addViewport(mCamera);

    // Adding a Viewport
    vp->setBackgroundColour(Ogre::ColourValue(0,0,0));

    mCamera->setAspectRatio(
                Ogre::Real(vp->getActualWidth()) /
                Ogre::Real(vp->getActualHeight()));
}

void ApplicationBase::setupListeners()
{
    mRoot->addFrameListener(this);
}
