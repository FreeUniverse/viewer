#ifndef __ApplicationBase_h_
#define __ApplicationBase_h_

#include <OgreRoot.h>

using std::size_t;
using std::string;

class ApplicationBase :
        public Ogre::FrameListener
{
public:
    ApplicationBase();
    virtual ~ApplicationBase();

    bool go();
protected:
    // Ogre::FrameListener
    virtual bool frameRenderingQueued(const Ogre::FrameEvent& evt);
    virtual void createScene();
    bool setup();
    void loadResourceConfig();
    void loadResource();
    bool setupConfig();
    void setupSceneManager();
    void setupViewing();
    virtual void setupListeners();
protected:
    Ogre::Root* mRoot;
    Ogre::String mResourcesCfg;
    Ogre::String mPluginsCfg;
    Ogre::RenderWindow* mWindow;
    Ogre::SceneManager* mSceneMgr;
    Ogre::Camera* mCamera;
    Ogre::SceneNode* mCamNode;

    string startTime;
};

#endif // #ifndef __ApplicationBase_h_
