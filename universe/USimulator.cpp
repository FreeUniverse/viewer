#include "USimulator.h"

#include <iostream>
#include <thread>

USimulator::USimulator()
{
    mDataBridge = DataBridgeFactory::create("REST");
    mSpeed = 20;
}

USimulator::~USimulator()
{
    delete mDataBridge;
}

void USimulator::setTime(const string &str_time)
{
    time = date(str_time);
    mUniverses.free(time - 1);
    planets();
    prepare(&time);
}

string USimulator::getTimeStr() const
{
    return time.str();
}

/* 告知時間變化
 * @Return: 星球坐標是否應當變更
 */
bool USimulator::injectTimePulse(float delta)
{
    static float timer = 0;
    if (mSpeed == 0)
        return false;
    timer -= delta;
    if (timer <= 0) {
        timer = 1.f / mSpeed;
        shift();
        return true;
    }
    return false;
}

const Universe USimulator::universe()
{
    return mUniverses[time];
}

const set<string> USimulator::planets()
{
    std::lock_guard<std::mutex> lck(mt_p);
    if (mPlanet.size() == 0) {
        mPlanet = mDataBridge->getPlanetList();
        mInfos = mDataBridge->getAvailablePlanets();
    }
    return mPlanet;
}

const PlanetInfo &USimulator::info(const string &planet)
{
    for (const PlanetInfo& info : mInfos) {
        if (info.name == planet)
            return info;
    }
    throw std::runtime_error("星球信息不存在:" + planet);
}

int USimulator::speed() const
{
    return mSpeed;
}

void USimulator::setSpeed(int speed)
{
    mSpeed = speed;
}

void USimulator::shift()
{
    mUniverses.free(time);
    time += 1;
    prepare();
}

void USimulator::prepare(const date *t)
{
    auto f = [=](const date time) {
        set<date>::iterator it;
        {
            std::lock_guard<std::mutex> lck(mt_p);
            if (mPending.find(time) != mPending.end())
                return;
            std::pair<set<date>::iterator,bool> p = mPending.insert(time);
            if (p.second)
                it = p.first;
            else
                throw std::runtime_error("將待獲取時間插入待獲取列表失敗");
        }
        const Universe& universe = mDataBridge->fetchUniverse(time.str(), planets());
        mUniverses.insert(time, universe);
        {
            std::lock_guard<std::mutex> lck(mt_p);
            mPending.erase(it);
        }
    };
    if (t != NULL)
        f(*t);
    std::lock_guard<std::mutex> lck(mt_p);
    for (size_t i = mUniverses.size(); i < mSpeed; i++) {
        std::thread td(f, time + i);
        td.detach();
    }
}
