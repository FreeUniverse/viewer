#ifndef DATABRIDGE_REST_H
#define DATABRIDGE_REST_H

#include "databridge.h"
#include "data_structure.h"
#include <vector>
#include <boost/asio.hpp>

using std::vector;
using boost::asio::ip::tcp;

class DataBridge_REST
        : public DataBridge
{
public:
    DataBridge_REST(const string waiter_ip, const int waiter_port);
    set<string> getPlanetList();
    set<PlanetInfo> getAvailablePlanets();
    Universe fetchUniverse(string time, set<string> planet);
private:
    string sendGETRequest(string url);
    PlanetInfo getPlanetInfo(string name);
    boost::asio::io_service io_service;
    set<string> mPlanetList;
    set<PlanetInfo> mPlanetInfos;
};

#endif // DATABRIDGE_REST_H
