#ifndef UNIVERSESIMULATOR_H
#define UNIVERSESIMULATOR_H

#include <string>
#include <mutex>
#include <set>
#include <map>
#include "databridge.h"
#include "date.h"
#include "ucluster.h"

using std::string;
using std::set;
using std::map;

class USimulator
{
public:
    USimulator();
    ~USimulator();

    void setTime(const string &str_time);
    string getTimeStr() const;
    bool injectTimePulse(float delta);
    const Universe universe();
    const set<string> planets();
    const PlanetInfo& info(const string& planet);

    int speed() const;
    void setSpeed(int speed);

protected:
    void shift();
    void prepare(const date *time = NULL); // 獲得新的星球坐標
    DataBridge* mDataBridge;
    int mSpeed;
    date time;
    set<string> mPlanet;
    set<PlanetInfo> mInfos;
    ucluster mUniverses;
    set<date> mPending;
    std::mutex mt_p;
};

#endif // UNIVERSESIMULATOR_H
