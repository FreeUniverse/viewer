#include "databridge.h"

#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/ptree.hpp>
#include <sstream>

set<string> DataBridge::parsePlanetList(string data)
{
    using namespace boost::property_tree;
    ptree pt;
    {
        std::stringstream ss;
        ss << data;
        read_json<ptree>(ss, pt);
    }
    set<string> planet_list;
    for (const ptree::value_type& child : pt) {
        string name = child.second.data();
        planet_list.insert(name);
    }
    return planet_list;
}

Universe DataBridge::parseUniverse(string time, string data)
{
    using namespace boost::property_tree;
    ptree pt;
    {
        std::stringstream ss;
        ss << data;
        read_json<ptree>(ss, pt);
    }
    Universe universe;
    universe.time = time;
    for (ptree::value_type &v : pt) {
        Planet pinfo;
        coordinate pcoord;
        const ptree& name = v.second.get_child("Name");
        pinfo.name = name.data();
        const ptree& coord = v.second.get_child("Coord");
        pcoord.x = coord.get<float>("X");
        pcoord.y = coord.get<float>("Y");
        pcoord.z = coord.get<float>("Z");
        pinfo.coord = pcoord;
        universe.planets.push_back(pinfo);
    }
    return universe;
}

PlanetInfo DataBridge::parsePlanet(string data)
{
    using namespace boost::property_tree;
    ptree pt;
    {
        std::stringstream ss;
        ss << data;
        read_json<ptree>(ss, pt);
    }
    PlanetInfo info;
    info.name = pt.get<string>("Name");
    info.revise_date = pt.get<string>("Revised");
    info.radius = pt.get<float>("Radius");
    info.mass = pt.get<string>("Mass");
    info.center = pt.get<string>("Center");
    return info;
}

set<PlanetInfo> DataBridge::parseAllPlanets(string data)
{
    using namespace boost::property_tree;
    ptree pt;
    {
        std::stringstream ss;
        ss << data;
        read_json<ptree>(ss, pt);
    }
    set<PlanetInfo> planets;
    for (const ptree::value_type& child : pt) {
        ptree pt_info = child.second;
        PlanetInfo info;
        info.name = pt_info.get<string>("Name");
        info.revise_date = pt_info.get<string>("Revised");
        info.radius = pt_info.get<float>("Radius");
        info.mass = pt_info.get<string>("Mass");
        planets.insert(info);
    }
    return planets;
}

#include "databridge_tcp.h"
#include "databridge_rest.h"
DataBridge *DataBridgeFactory::create(string protocal, const std::__cxx11::string waiter_ip, const int waiter_port)
{
    if (protocal == "TCP")
        return new DataBridge_TCP(waiter_ip, waiter_port);
    else if (protocal == "REST")
        return new DataBridge_REST(waiter_ip, waiter_port);
    else
        throw std::domain_error("未知協議類型");
}
