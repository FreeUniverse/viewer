#ifndef DATA_STRUCTURE_H
#define DATA_STRUCTURE_H

#include <string>
#include <vector>

using std::vector;
using std::string;

struct coordinate {
    float x, y, z;
};

struct Planet {
    string name;
    coordinate coord;
};

struct Universe {
    string time;
    vector<Planet> planets;
    static const string str(const Universe universe);
    void swap(Universe& first, Universe& second);
    Universe& operator=(Universe other);
};

struct PlanetInfo {
    string name;
    string revise_date;
    float radius;
    string mass;
    string center;
    bool operator<(const PlanetInfo other) const {
        return this->name < other.name;
    }
};

#endif // DATA_STRUCTURE_H
