#include "databridge_tcp.h"
#include <boost/property_tree/json_parser.hpp>
#include <sstream>
#include <iostream>
#include <string>

DataBridge_TCP::DataBridge_TCP(const string waiter_ip, const int waiter_port)
{
    this->waiter_ip =  waiter_ip;
    this->waiter_port = waiter_port;
}

set<string> DataBridge_TCP::getPlanetList()
{
    if (mPlanetInfos.size() == 0)
        getAvailablePlanets();
    set<string> planetList;
    for (const PlanetInfo &info : mPlanetInfos) {
        string name = info.name;
        planetList.insert(name);
    }
    return planetList;
}

set<PlanetInfo> DataBridge_TCP::getAvailablePlanets()
{
    if (mPlanetInfos.size() == 0) {
        tcp::socket socket(ioservice);
        tcp::endpoint endpoint(boost::asio::ip::address_v4::from_string(waiter_ip), waiter_port);
        socket.connect(endpoint);
        set<string> targets;
        targets.insert("Planets");
        string request = mkConsultRequest(targets);
        socket.write_some(boost::asio::buffer(request));
        string recv_data = readData(socket);
        mPlanetInfos = parseConsultResponse(recv_data);
    }
    return mPlanetInfos;
}

Universe DataBridge_TCP::fetchUniverse(string time, set<string> planet)
{
    tcp::socket socket(ioservice);
    tcp::endpoint endpoint(boost::asio::ip::address_v4::from_string(waiter_ip), waiter_port);
    socket.connect(endpoint);
    string request = mkUniverseRequest(time, planet);
    socket.write_some(boost::asio::buffer(request));
    string recv_data = readData(socket);
    return parseUniverseResponse(recv_data);
}

string DataBridge_TCP::readData(tcp::socket &socket)
{
    std::vector<char> buf(10240);
    auto buffer = boost::asio::buffer(buf);
    socket.receive(buffer);
//    socket.read_some(buffer);
    string recv_data = boost::asio::buffer_cast<char*>(buffer);
    return recv_data;
}

pair<string, ptree> DataBridge_TCP::parseResponse(string data, string expected)
{
    using namespace boost::property_tree;
    ptree p0;
    std::stringstream ss;
    ss << data;
    read_json<ptree>(ss, p0);
    const string type = p0.get_child("Type").data();
    if (expected != "") {
        if (type != expected) {
            throw std::range_error("收到的消息類型非預期類型");
        }
    }
    ptree p1 = p0.get_child("Response");
    return pair<string, ptree>(type, p1);
}

Universe DataBridge_TCP::parseUniverseResponse(string data)
{
    pair<string, ptree> ret = parseResponse(data, "Universe");
    ptree pt = ret.second;
    Universe universe;
    universe.time = pt.get_child("Time").data();
    ptree &planets_ptree = pt.get_child("Planets");
    for (ptree::value_type &v : planets_ptree) {
        Planet pinfo;
        coordinate pcoord;
        const ptree& name = v.second.get_child("Name");
        pinfo.name = name.data();
        const ptree& coord = v.second.get_child("Coord");
        pcoord.x = coord.get<float>("X");
        pcoord.y = coord.get<float>("Y");
        pcoord.z = coord.get<float>("Z");
        pinfo.coord = pcoord;
        universe.planets.push_back(pinfo);
    }
    return universe;
}

set<PlanetInfo> DataBridge_TCP::parseConsultResponse(string data)
{
    pair<string, ptree> ret = parseResponse(data, "Consult");
    ptree pt = ret.second;
    set<PlanetInfo> planets;
    for (const ptree::value_type& child : pt.get_child("Planets")) {
        ptree pt_info = child.second;
        PlanetInfo info;
        info.name = pt_info.get<string>("Name");
        info.revise_date = pt_info.get<string>("Revised");
        info.radius = pt_info.get<float>("Radius");
        info.mass = pt_info.get<string>("Mass");
        planets.insert(info);
    }
    return planets;
}

string DataBridge_TCP::mkRequest(string type, ptree pt)
{
    ptree req;
    req.put("Type", type);
    req.put_child("Request", pt);
    std::stringstream ss;
    write_json(ss, req);
    return ss.str() + "\n";
}

string DataBridge_TCP::mkUniverseRequest(string time, set<string> planet)
{
    ptree pt, pp;
    pt.put("Time", time);
    for (string p : planet) {
        ptree child;
        child.put("", p);
        pp.push_back(std::make_pair("", child));
    }
    pt.add_child("Planet", pp);
    return mkRequest("Universe", pt);
}

string DataBridge_TCP::mkConsultRequest(set<string> target)
{
    using boost::property_tree::ptree;
    ptree pt;

    ptree children;
    for (string t : target) {
        ptree child;
        child.put("", t);
        children.push_back(std::make_pair("", child));
    }

    pt.add_child("Target", children);

    return mkRequest("Consult", pt);
}
