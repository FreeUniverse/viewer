#ifndef DATE_H
#define DATE_H

#include <string>
#include <sstream>

using std::string;

struct date
{
    long year;
    char month;
    char day;

    static bool is_leap_year(const long year) {
        if (year % 100 != 0)
            if (year % 400 == 0)
                return true;
            else
                return false;
        else
            if (year % 4 == 0)
                return true;
            else
                return false;
    }
    inline static bool is_leap_year(const date& d) {
        return is_leap_year(d.year);
    }
    static int days_of_month(long year, char month) {
        static const int day_of_month[12] = {
            31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};
        int num;
        num = day_of_month[month - 1];
        if ((month == 2) && is_leap_year(year))
            num += 1;
        return num;
    }
    inline static int days_of_month(const date& d) {
        return days_of_month(d.year, d.month);
    }

    date(long year = 1, char month = 1, char day = 1) {
        this->year = year;
        this->month = month;
        this->day = day;
    }
    date(const string& str_date) {
        std::stringstream ss(str_date);
        string s;
        if (std::getline(ss, s, '-')) {
            year = std::stol(s);
            if (std::getline(ss, s, '-')) {
                month = char(std::stoi(s));
                if (std::getline(ss, s, '-')) {
                    day = char(std::stoi(s));
                }
            }
        }
    }
    date(const date& other) {
        this->year = other.year;
        this->month = other.month;
        this->day = other.day;
    }
    string str() const {
        string s = "";
        s += std::to_string(year);
        s += "-";
        s += std::to_string(month);
        s += "-";
        s += std::to_string(day);
        return s;
    }
    date operator+(int days) const {
        if (days < 0)
            return *this - (-days);
        date ret(*this);
        ret.day += days;
        int num = days_of_month(ret);
        while (ret.day > num) {
            ret.month += 1;
            ret.day -= num;
            if (ret.month > 12) {
                ret.year += 1;
                ret.month -= 12;
            }
            num = days_of_month(ret);
        }
        return ret;
    }
    date operator-(int days) const {
        if (days < 0)
            return *this + (-days);
        date ret(*this);
        ret.day += days;
        while (ret.day <= 0) {
            ret.month -= 1;
            if (ret.month <= 0) {
                ret.year -= 1;
                ret.month += 12;
            }
            ret.day += days_of_month(ret);
        }
        return ret;
    }
    date& operator+=(int days) {
        *this = *this + days;
        return *this;
    }
    date& operator-=(int days) {
        *this = *this - days;
        return *this;
    }
    bool operator==(const date& other) const {
        if (day != other.day)
            return false;
        if (month != other.month)
            return false;
        if (year != other.year)
            return false;
        return true;
    }
    bool operator<(const date& other) const {
        if (year < other.year)
            return true;
        if (year > other.year)
            return false;
        if (month < other.month)
            return true;
        if (month > other.month)
            return false;
        if (day < other.day)
            return true;
        return false;
    }
    bool operator<=(const date& other) const {
        return (*this < other) || (*this == other);
    }
};

#endif // DATE_H
