#include "data_structure.h"

#include <string>
#include <sstream>
#include <boost/format.hpp>

using std::string;
using boost::format;

const string Universe::str(const Universe universe)
{
    std::stringstream ss;
    ss << format("time: %s\n") % universe.time;
    for (size_t i = 0; i < universe.planets.size(); i++) {
        const Planet planet = universe.planets[i];
        ss << format(" name: %d\n") % planet.name;
        ss << format("  coord: x:%f y:%f z:%f\n") % planet.coord.x % planet.coord.y % planet.coord.z;
    }
    return ss.str();
}

void Universe::swap(Universe& first, Universe& second)
{
    using std::swap;
    swap(first.time, second.time);
    swap(first.planets, second.planets);
}

Universe& Universe::operator=(Universe other) {
    swap(*this, other);
    return *this;
}
