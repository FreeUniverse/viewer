#project(free_universe)
cmake_minimum_required(VERSION 3.1)
set(CMAKE_CXX_STANDARD 11)

set(CMAKE_MODULE_PATH "/usr/lib/OGRE/cmake/;${CMAKE_MODULE_PATH}")

find_package(OGRE REQUIRED)

include_directories(
    ${OGRE_INCLUDE_DIRS}
    )

aux_source_directory(. SRC_LIST)
add_library(3rd_party SHARED ${SRC_LIST})

target_link_libraries(3rd_party ${OGRE_LIBRARIES})
